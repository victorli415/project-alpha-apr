from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required(login_url="")
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    print(project)
    context = {
        "list_of_project_objects": project,
    }
    return render(request, "projects/list.html", context)


@login_required(login_url="")
def detail_project(request, id):
    task = Project.objects.get(id=id)
    context = {
        "list_of_task_objects": task,
    }
    return render(request, "projects/detail.html", context)


@login_required(login_url="")
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = CreateProjectForm()
    context = {
        "create_project_form": form,
    }
    return render(request, "projects/create_project.html", context)
