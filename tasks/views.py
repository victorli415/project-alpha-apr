from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import CreateTaskForm


# Create your views here.


@login_required(login_url="")
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = CreateTaskForm()
    context = {
        "create_task_form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required(login_url="")
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "list_of_task_objects": task,
    }
    return render(request, "tasks/list.html", context)
